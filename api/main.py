import json
from random import randint, random
import requests
from fastapi import FastAPI
from pydantic import BaseModel



class Game:
    
    def __init__(self) -> None:
        self.posibilities=["salut","man","hello"]
        self.mots=[]
        self.solve=self.posibilities[randint(0,len(self.posibilities)-1)]
        

    def add_mot(self,mot:str):
        self.mots.append(mot)
        

    def get_mots(self):
        return self.mots

    def is_equals(self,word:str):
        return self.solve==word
    
    

app = FastAPI()




app=FastAPI()
game=Game()

@app.get("/")
def read_root():
    return {"hello":"root"}


@app.get("/mots")
def get_request_mot():
    return game.mots


@app.get("/GUESS/{guess}")
def try_word(guess:str):
    if(game.is_equals(guess)):
        return {"you" :"win"}
    else:
        game.add_mot(guess)
        return game.get_mots
    



