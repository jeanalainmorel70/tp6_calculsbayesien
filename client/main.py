
import os
import time
import requests
from dotenv import load_dotenv

load_dotenv()

URL=os.getenv("URL")


def acceuil():
    r=requests.get(URL)
    return r.json()


def make_a_guess(word:str):
    r=requests.get(URL+"GUESS/"+word)
    return r.json()

def see_oldest_proposition():
    r=requests.get(URL+"mots")
    return r.json()


running=True

while(running):
    print("choose action : \n 1.acceuil \n 2.GUESS \n 3.see oldest proposition \n 4.QUITT ")
    opt=int(input("tape:"))

    if(opt==1):
        print(acceuil())
    elif(opt==2):
        proposition=input("GUESS :")
        print(make_a_guess(proposition))

    elif (opt==3):
        print(see_oldest_proposition())
    elif(opt==4):

        running=False
    
    time.sleep(1)




#def get_mots():
#    return 1
#
#
#def add_mot():
#    return 1